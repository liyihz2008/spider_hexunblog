和讯博客爬虫

爬虫难度一般，只是博文的点击量和评论数以及作者信息为异步加载，需要使用其他的请求获取
此作业实现了两版：
第一版使用高效的请求方式进行博文以及点击量评论数的获取：高效，但是需要一次插入一次更新数据库
第二版使用了selenium延迟获取博文信息，等整体加载完成后，再获取网页数据：可以一次性拿到所有数据

此版本为第一版：
难点：
1、获取博文ID，博客ID
   网页的JS中有一句：

   showArticleComments("25914240", "113881877", "18594907", 1);

   可以使用正则表达试从中获取三个数据：用户ID，博文ID，博客ID


2、获取评论数和点击量
   请求：http://click.tool.hexun.com/click.aspx?articleid={article_id}&blogid={blog_id}
   方法：GET
   响应：document.getElementById("articleClickCount").innerHTML = 857;
        document.getElementById("articleCommentCount").innerHTML = 0;
   然后使用正则表达试获取数字


执行命令：
scrapy crawl spider_hexunblog



