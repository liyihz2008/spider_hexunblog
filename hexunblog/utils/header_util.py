# -*- coding: utf-8 -*-
import random

from hexunblog.service.article_service import ArticleInfoService


class HeaderUtil:

    agent_pool = ["Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36"]

    def __init__(self):
        pass

    '''
    伯乐在线的数据获取，头里需要添加一个host名称，这个Host可以从URL里获取信息进行组织
    '''
    @classmethod
    def get_common_request_header(cls, url):
        host_name = ArticleInfoService.get_host_from_url(url)
        referer = url.split('?')[0].split('#')[0]
        if not host_name:
            host_name = "blog.hexun.com"
        agent = random.choice(cls.agent_pool)
        headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2",
            "Connection": "keep-alive",
            "Host": host_name,
            "Referer": referer,
            "Upgrade-Insecure-Requests": 1,
            "User-Agent": agent,
        }
        return headers

    '''
    伯乐在线的数据获取，头里需要添加一个host名称，这个Host可以从URL里获取信息进行组织
    '''

    @classmethod
    def get_comment_request_header(cls, blog_url, url):
        host_name = ArticleInfoService.get_host_from_url(url)
        if not host_name:
            host_name = "blog.hexun.com"
        agent = random.choice(cls.agent_pool)
        headers = {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "zh-CN,zh;q=0.9",
            "Connection": "keep-alive",
            "Host": host_name,
            "Cookie": 'HEXUN_COM_MEDIA_PLAYSTATE=1',
            "Referer": blog_url,
            "User-Agent": agent,
        }
        return headers