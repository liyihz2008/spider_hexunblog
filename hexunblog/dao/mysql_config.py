#!/usr/bin/env python
# -*- coding: utf-8 -*-
db_config = {
    'pool_name': 'jobbole_pool',
    'host': 'o2oa.io',
    'port': 3306,
    'user': '****',
    'password': '****',
    'database': '****',
    'pool_resize_boundary': 32,
    'enable_auto_resize': True,
}
