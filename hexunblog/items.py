# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class HexunblogItem(scrapy.Item):
    article_id = scrapy.Field()
    blog_id = scrapy.Field()
    user_id = scrapy.Field()
    original = scrapy.Field()
    title = scrapy.Field()
    click_count = scrapy.Field()
    comment_count = scrapy.Field()
    author = scrapy.Field()
    tag_name = scrapy.Field()
    class_name = scrapy.Field()
    publish_date = scrapy.Field()
    url = scrapy.Field()

