# -*- coding: utf-8 -*-
from hexunblog.dao.article_dao_db import ArticleInfoDaoDB


class HexunblogPipeline(object):

    def process_item(self, item, spider):

        # 如果存在标题，说明是新的博文，需要进行博文存储
        if item["title"]:
            ArticleInfoDaoDB.save(
                item["article_id"],
                item["blog_id"],
                item["user_id"],
                item["original"],
                item["title"],
                item["click_count"],
                item["comment_count"],
                item["author"],
                item["tag_name"],
                item["class_name"],
                item["url"],
                item["publish_date"]
            )
        # 如果不存在标题，说明是分析的点击量和评论数，需要进行博文更新
        elif not item["title"]:
            if int(item["click_count"]) > 0 or int(item["comment_count"]) > 0:
                ArticleInfoDaoDB.update_click_comment_count(
                    item["article_id"],
                    item["click_count"],
                    item["comment_count"],
                )
